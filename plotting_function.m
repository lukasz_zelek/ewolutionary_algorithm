% funkcja rysujaca wykresy - na potrzeby testow
function [] = plotting_function(DefRedoubtsList, ForcesAbilitySumList)
    axis4 = get_SIZE_global + 1;
    N = get_global_N + 1;
    Y_axis1 = get_BASE_global * axis4*0.06;
    Y_axis2 = get_BASE_global * axis4*2;
    sizeOfList = size(DefRedoubtsList);
    dim2 = sizeOfList(2);
    domain = 1:dim2;
    figure(1);
    subplot(3,1,1);
    bar(domain, DefRedoubtsList);
    axis([0 N 0 axis4]);
    xlabel('Iteracje'), ylabel('Liczba obronionych redut'), title('Wykres najgorszych osobnik�w w kolejnych iteracjach')
    subplot(3,1,2);
    bar(domain, ForcesAbilitySumList);
    axis([0 N 0 Y_axis1]);
    xlabel('Iteracje'), ylabel('Minimalna warto�� obronna'), title('Wykres najgorszej warto�ci w kolejnych iteracjach')
    subplot(3,1,3);
    bar(domain, DefRedoubtsList.*ForcesAbilitySumList);
    axis([0 N 0 Y_axis2]);
    xlabel('Iteracje'), ylabel('Iloczyn liczba*obronno��'), title('Wykres liczba*obronno�� w kolejnych iteracjach')
end

