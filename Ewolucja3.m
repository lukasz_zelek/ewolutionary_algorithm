% clc
clearvars -except vector_of_solutions z vector_of_solutions_points_min vector_of_solutions_points_max start_time size_of_problem size_of_population number_of_iterations 

% zmienne glowne
% odkomentowac w razie potrzeby testow i zakomentowac przeciwne:     
global SIZE_global;  % Ilosc redut wraz z zapleczem
% SIZE_global = size_of_problem; 
SIZE_global = 18; 

global POPULATION_SIZE; % Rozmiar populacji
% POPULATION_SIZE = size_of_population;
POPULATION_SIZE = 20;

global BASE;  %Liczba jednostek w bazie
BASE = 40;

global N; % Ilo�� iteracje
% N = number_of_iterations;
N = 2000;

global solution_num; % Zmienna globalna konieczna dla testow
factor = 1;
% odkomentowac w razie potrzeby testow i zakomentowac przeciwne:
% init_forces = [BASE, 6, 7, 8, 9, 7, 6, 7, 8, 9, 7, 6, 9]; 
init_forces = create_init_forces;
% min_def_req = [0, 31, 2, 1, 17, 30, 10, 13, 15, 14, 11, 7, 10];
min_def_req = create_def_req_forces;

% tworzenie populacji startowej - proces losowy
population = init_generation_function(init_forces);
SIZE = size(population);
Size = SIZE(1);

% macierze pomocnicze
Num_of_def_redoubts_matrix = zeros(1, Size);
Num_of_forces_ability_matrix = zeros(Size, SIZE_global);
Num_of_forces_ability_matrix_sum = zeros(Size, 1);
% score = zeros(1, Size);

% zmienne do wykresow
NumOfDefRedoubtsPlot = zeros(1,N);
ForcesAbilitySumPlot = zeros(1,N);
ForcesAbilitySumPlotMax = zeros(1,N);

% petla iteracji
for evolution_number = 1:N
    SIZE = size(population);
    Size = SIZE(1);
    
    for i = 1:Size
        evolution_function3(population(i, :), init_forces, min_def_req);
        Num_of_def_redoubts_matrix(i) = get_Global_Score;
        Num_of_forces_ability_matrix_sum(i) = sum(get_Global_Forces_ability);
    end

    % etap selekcji rodzicow
    pop_selected = selection_function(population, Num_of_def_redoubts_matrix, Num_of_forces_ability_matrix_sum, factor); 
    % etap krzyzowania i mutacji
    if evolution_number < N
        population = crossing_function(pop_selected, init_forces);
    else
        population = pop_selected;
        SIZE = size(population);
        Size = SIZE(1);
        Num_of_forces_ability_matrix_sum = zeros(1, Size);
        Num_of_def_redoubts_matrix = zeros(1, Size);
        Num_of_forces_ability_matrix = zeros(1, Size);
        
        for i = 1:Size
            evolution_function3(population(i, :), init_forces, min_def_req);
            Num_of_forces_ability_matrix_sum(i) = sum(get_Global_Forces_ability);
        end
    end
    % zmienne do wykresow
    stepForcesAbility = min(Num_of_forces_ability_matrix_sum);
    stepForcesAbilityMax = max(Num_of_forces_ability_matrix_sum);
    stepNumOfDefRedoubts = max(Num_of_def_redoubts_matrix);
    stepNumOfDefRedoubtsMin = min(Num_of_def_redoubts_matrix);
    ForcesAbilitySumPlot(evolution_number) = stepForcesAbility;
    ForcesAbilitySumPlotMax(evolution_number) = stepForcesAbilityMax;
    NumOfDefRedoubtsPlot(evolution_number) = stepNumOfDefRedoubts;
    NumOfDefRedoubtsPlotMin(evolution_number) = stepNumOfDefRedoubtsMin;
end

% min(Num_of_def_redoubts_matrix);
[Max, I] = max(Num_of_forces_ability_matrix_sum);
The_Chosen_One_Population = population(I, :);

% The_Chosen_One_Population = [0 47 6 7 19 49];

evolution_function3(The_Chosen_One_Population, init_forces, min_def_req);
Num_of_def_redoubts = get_Global_Score;
Forces_new = get_Global_Forces_new;
Forces_ability = get_Global_Forces_ability;
Forces_ability_matrix_sum = Max;
solution_num = [NumOfDefRedoubtsPlot; ForcesAbilitySumPlot; ForcesAbilitySumPlotMax;NumOfDefRedoubtsPlotMin];
% odkomentowac jezeli program ma rysowac wykresy
% plotting_function(NumOfDefRedoubtsPlot,ForcesAbilitySumPlot);















