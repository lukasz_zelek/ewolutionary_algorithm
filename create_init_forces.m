% automatyczne tworzenie wektora wag jednostek poczatkowych - sposob
% realizacji dowolny
function [init_matrix] = create_init_forces()
size = get_SIZE_global;
matrix = zeros(1, size);
matrix(1) = get_BASE_global;
for p = 2:get_SIZE_global
    switch mod(p,5) 
            case 0
                matrix(p) = 4;
            case 1
                matrix(p) = 7;
            case 2
                matrix(p) = 5;
            case 3
                matrix(p) = 8;
            case 4
                matrix(p) = 3;
            case 5
                matrix(p) = 9;
    end
end
init_matrix = matrix;
end