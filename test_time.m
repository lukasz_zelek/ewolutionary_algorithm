lower_bound_problem_size = 5;
upper_bound_problem_size = 100;

size_vector = lower_bound_problem_size:upper_bound_problem_size;
result_vector = zeros(size(size_vector));

j = 1;
for i=size_vector
    i
    result_vector(j) = test_function(i,20,2000);
    j = j + 1;
end


plot(size_vector, result_vector)
xlabel("Liczba redut"), ylabel("Czas wykonywania algorytmu"), 
title("Z�o�ono�� czasowa algorytmu w zale�no�ci od rozmiaru problemu")
legend("Czas wykonania w sekundach", 'Location','southeast');
grid on;

%%
% liczba iteracji zmienna
lower_bound_problem_size = 100;
upper_bound_problem_size = 5000;

size_vector = linspace(100, 5000, 50);
result_vector = zeros(size(size_vector));

j = 1;
for i=size_vector
    i
    result_vector(j) = test_function(16,20,i);
    j = j + 1;
end


plot(size_vector, result_vector)
xlabel("Liczba iteracji"), ylabel("Czas wykonywania algorytmu"), 
title("Z�o�ono�� czasowa algorytmu w zale�no�ci od liczby iteracji")
legend("Czas wykonania w sekundach", 'Location','southeast');
grid on;
%%

size_vector = 10:50
result_vector = zeros(size(size_vector));
j = 1;
for i=size_vector
    i
    result_vector(j) = test_function(16,i,2000);
    j = j + 1;
end

%%
plot(size_vector, result_vector)
xlabel("Rozmiar populacji"), ylabel("Czas wykonywania algorytmu"), 
title("Z�o�ono�� czasowa algorytmu w zale�no�ci od rozmiaru populacji")
legend("Czas wykonania w sekundach", 'Location','southeast');
grid on;

%% 
% wykres 2D czas wykonania

test_r_vec = 5:55;
test_p_vec = 5:55;
result_matrix = zeros(size(test_r_vec), size(test_r_vec));

a=1;
b=1;
for i = test_r_vec
    a
    for j = test_p_vec
        result_matrix(a,b) = test_function(i,j,2000);
        b=b+1;
    end
    a=a+1;
end

% plot(size_vector, result_vector)
% xlabel("Rozmiar populacji"), ylabel("Czas wykonywania algorytmu"), 
% title("Z�o�ono�� czasowa algorytmu w zale�no�ci od rozmiaru populacji")
% legend("Czas wykonania w sekundach", 'Location','southeast');
% grid on;

% analogicznie dla pami�ci u�ytej przez program
