# ewolutionary_algorithm
# Matlab code for calculating best individuals from set of generated populations.

# Decription of m-files(functions)
- crossing_function()
- ewolution_function3()
- get_Global_Actual_changes() - getter
- get_Global_cost() - getter
- get_Global_Forces_ability() - getter
- get_Global_Forces_new() - getter
- get_Global_Num_of_def_redoubts() - getter
- get_Global_Population() - getter
- get_Global_Score() - getter
- init_generation_function()
- mutate_function()
- selection_function()

# Other m-files
- Ewolucja3
