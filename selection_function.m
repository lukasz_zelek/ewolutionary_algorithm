function [population_selected] = selection_function(population, Num_of_def_redoubts_matrix, Num_of_forces_ability_matrix_sum, factor)

    SIZE = size(population);
    Size = SIZE(1);
    
    Sol_matrix = zeros(Size);
    
    Max_num_def = max(Num_of_def_redoubts_matrix);
    Max_score = max(Num_of_forces_ability_matrix_sum);
    Min_score = min(Num_of_forces_ability_matrix_sum);
    
    
    diff = (Max_score - Min_score) * factor;
    Score_threshold = Max_score - diff;
    
    number_of_max_def = 0;
    
    % wyliczanie wartosci funkcji celu dla kazdego osobnika populacji
    
    for i = 1:Size
        if Num_of_def_redoubts_matrix(i) == Max_num_def
            Sol_matrix(i) = (Num_of_def_redoubts_matrix(i)) * Num_of_forces_ability_matrix_sum(i);
        else
           Sol_matrix(i) = Num_of_def_redoubts_matrix(i) * Num_of_forces_ability_matrix_sum(i); 
        end
    end
    % sortowanie osobnikow wg wartosci funkcji celu
    [out,idx] = sort(Sol_matrix);
    
    % wybor 10% najlepszych - rodzicow
    Number_10 = floor(0.1 * Size);
    idx_10 = idx(Size - Number_10 : Size);
    population_selected = zeros(Number_10, SIZE(2));
    pos_vector = zeros(1, Number_10);
    
    for i = 1:Number_10
        population_selected(i, :) = population(idx_10(i), :);
    end
end














