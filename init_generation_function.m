% losowa generacja populacji poczatkowej
function [population] = init_generation_function(initial_forces)
    SIZE = get_Global_POPULATION_SIZE;
    SIZE_global = get_SIZE_global;
    population = zeros(SIZE, SIZE_global);

    for i = 1:SIZE
        base = get_BASE_global;      % liczba jednostek w bazie
        
        base_vector = zeros(1, SIZE_global - 1);
        base_position = linspace(1, (SIZE_global - 1), (SIZE_global - 1));
        
        for n = 1:SIZE_global - 1
            number = randi(length(base_position));
            base_vector(n) = base_position(number);
            base_position(number) = [];
        end
        
        for j = 1:SIZE_global
            if j == 1
                population(i, j) = 0;
                
            elseif j < SIZE_global
                num = base_vector(j - 1);
                if base == 0
                    forces = 0;
                else
                    forces = randi([0, base]);
                end
                population(i, num + 1) = initial_forces(j) + forces;
                base = base - forces;
                
            else 
                num = base_vector(j - 1);
                population(i, num + 1) = initial_forces(j) + base;
            end
        end
    end
    
    for i = 1:50
    end
    
end






