function num_of_def = evolution_function3(Changed_forces, forces, min_def_req)

% odkomentowac w razie testowania na ponizszym przykladzie i zakomentowac
% przeciwne
% Movement_cost = ones(6, 6)*3;
% Movement_cost = [0.0, 1.2, 1.1, 1.5, 1.1, 1.3, 1.2, 1.1, 1.5, 1.1, 1.3, 1.1, 1.7;
%                  1.2, 0.0, 1.3, 1.4, 0.7, 0.9, 1.2, 1.1, 1.5, 1.1, 1.3, 1.1, 1.7;
%                  1.1, 1.2, 0.0, 0.8, 1.0, 0.8, 1.2, 1.1, 1.5, 1.1, 1.3, 1.1, 1.7; % <-- 1.3 na 1.2
%                  1.5, 1.4, 0.8, 0.0, 0.8, 1.4, 1.2, 1.1, 1.5, 1.1, 1.3, 1.1, 1.7;
%                  0.9, 1.7, 1.6, 0.6, 0.0, 1.7, 1.2, 1.1, 1.5, 1.1, 1.3, 1.1, 1.7;
%                  1.3, 0.9, 0.8, 1.4, 1.7, 0.0, 1.2, 1.1, 1.5, 1.1, 1.3, 1.1, 1.7;
%                  1.3, 0.9, 0.8, 1.4, 1.7, 1.2, 0.0, 1.1, 1.5, 1.1, 1.3, 1.1, 1.7;
%                  1.3, 0.9, 0.8, 1.4, 1.7, 1.2, 1.3, 0.0, 1.5, 1.1, 1.3, 1.1, 1.7;
%                  1.3, 0.9, 0.8, 1.4, 1.7, 1.2, 1.3, 1.1, 0.0, 1.1, 1.3, 1.1, 1.7;
%                  1.3, 0.9, 0.8, 1.4, 1.7, 1.2, 1.3, 1.1, 1.5, 0.0, 1.3, 1.1, 1.7;
%                  1.3, 0.9, 0.8, 1.4, 1.7, 1.2, 1.3, 1.1, 1.5, 1.1, 0.0, 1.1, 1.7;
%                  1.3, 0.9, 0.8, 1.4, 1.7, 1.2, 1.3, 1.1, 1.5, 1.1, 1.3, 0.0, 1.7;
%                  1.3, 0.9, 0.8, 1.4, 1.7, 1.2, 1.3, 1.1, 1.5, 1.1, 1.3, 1.1, 0.0];

% generowanie macierzy kosztow
Movement_cost = create_Movement_cost;
             
SIZE = size(Movement_cost);

Size = SIZE(1);
dist_const = 0.1; % wspolczynnik kosztow

% macierz odleglosci - odkomentowac w razie potrzeby i zakomentowac
% pozostale
% Distance =  [0.0, 2.0, 3.0, 2.2, 2.8, 1.7, 2.0, 3.0, 2.2, 2.8, 1.7, 2.2, 3.3;
%              2.0, 0.0, 1.9, 1.7, 2.7, 1.9, 2.0, 3.0, 2.2, 2.8, 1.7, 2.2, 3.3;
%              3.0, 1.9, 0.0, 1.8, 2.6, 3.8, 2.0, 3.0, 2.2, 2.8, 1.7, 2.2, 3.3;                        
%              2.2, 1.7, 1.8, 0.0, 1.8, 1.2, 2.0, 3.0, 2.2, 2.8, 1.7, 2.2, 3.3;
%              2.8, 1.7, 2.6, 1.8, 0.0, 1.7, 2.0, 3.0, 2.2, 2.8, 1.7, 2.2, 3.3;
%              1.7, 1.9, 3.8, 1.2, 1.7, 0.0, 2.0, 3.0, 2.2, 2.8, 1.7, 2.2, 3.3;
%              1.7, 1.9, 3.8, 1.2, 1.7, 1.5, 0.0, 3.0, 2.2, 2.8, 1.7, 2.2, 3.3;
%              1.7, 1.9, 3.8, 1.2, 1.7, 1.5, 2.0, 0.0, 2.2, 2.8, 1.7, 2.2, 3.3;
%              1.7, 1.9, 3.8, 1.2, 1.7, 1.5, 2.0, 3.0, 0.0, 2.8, 1.7, 2.2, 3.3;
%              1.7, 1.9, 3.8, 1.2, 1.7, 1.5, 2.0, 3.0, 2.2, 0.0, 1.7, 2.2, 3.3;
%              1.7, 1.9, 3.8, 1.2, 1.7, 1.5, 2.0, 3.0, 2.2, 2.8, 0.0, 2.2, 3.3;
%              1.7, 1.9, 3.8, 1.2, 1.7, 1.5, 2.0, 3.0, 2.2, 2.8, 1.7, 0.0, 3.3;
%              1.7, 1.9, 3.8, 1.2, 1.7, 1.5, 2.0, 3.0, 2.2, 2.8, 1.7, 2.2, 0.0] * dist_const; 
             
Distance = create_distance * dist_const;

num_of_def = 0;                     % <- liczba obronionych redut
score = zeros(1, Size);          % <- wektor wynikow obronnosci w redutach
                                    
                                    
const_first = 1;        % wartosci obronnosci dla pierwszej jednostki w reducie, kazda kolejna ma ten wspolczynnik nizszy
const_next = 0.01;  %O ile kazda nastepna jednostka w reducie ma nizszy wspolczynnik obronnosci w stosunku do poprzedniej;
tiredness = zeros(1, Size);        % macierz kar walecznosci jednostek w zwiazku z przebyta przez nie droga

                    
global Actual_changes
Actual_changes = zeros(Size);

global Forces_new
Forces_new = forces;

global Forces_ability
Forces_ability = score;

global Score
Score = 0;

global cost
cost = 0;

Forces_new = Changed_forces;
cost = sum(sum(sqrt(Actual_changes) .* Movement_cost));

% 2 petle obliczania strat/kosztow alokacji
for i = 1:Size                    % iteracja po rozmiarze populacji
    if Forces_new(i) >= forces(i)
        for m = 1:size(Actual_changes(i))
            if Actual_changes(m, i) > 0
                tiredness(i) = Actual_changes(m, i) * Distance(m, i);
            end
        end
    end
end
        
        


for i = 1:Size % iteracja po rozmiarze populacji
    for j = 1:Forces_new(i)
        Forces_ability(i) = Forces_ability(i) + (const_first - (j-1)*const_next);
    end
    
    Forces_ability(i) = Forces_ability(i) - tiredness(i);
        
    if Forces_ability(i) >= min_def_req(i)
        num_of_def = num_of_def - 1;
    end
end

Score = num_of_def;
end















